import express from 'express'
import webpack from 'webpack'
import WebpackDevServer from 'webpack-dev-server'
import conversations from './conversations'

// Start WebpackDevServer
const config = require("./webpack.config.js");
config.entry.app.unshift("webpack-dev-server/client?http://localhost:8080/");
const compiler = webpack(config);
const server = new WebpackDevServer(compiler, {

});
server.listen(8080);


// Add six random conversations on server start
for (let i = 0; i < 6; i++) {
  conversations.addRandomConversation()
}
conversations.initRandomConversationAdder()

// Start app server
const app = express()
app.get('/api/conversations', (req,res) => {
  res.send(conversations.getConversations())
})
app.use(express.static('public'));
app.listen(3000, () => {
  console.log('App is now running on http://localhost:3000/')
})
