## The Intro

This is a hypothetical messaging app that lets you flex your app-building muscles
by thinking an app in terms of components, and use one-way data flow to pass
messages between components. If you are new to any of this or React, I highly
suggest you read [this article](https://facebook.github.io/react/docs/thinking-in-react.html)
about how to rewire your thinking while you make a react app.

Please read the article once and try it out before attempting this exercise.

## The Messenger App

Facebook wants to create a new website for their Messenger platform and they
are planning to build it using react. They have a basic chat server set-up,
the frontend interface for the chat app needs to be built now.

### Getting the code:
Assuming you have already created an account at gitlab,start by just clicking on
the Fork button above to fork this repository to your own account. Then you can
clone that to your machine and start working on it.

Please do not clone this repo directly. We wont give you any permissions to push code to
this repo directly.

### Set up instructions:
1. You need to have `node` version 4.0 or higher.
2. Go to the project directory and run
  ```
  npm install
  ```
3. Start the server
  ```
  npm start
  ```
4. This should start the server at http://localhost:3000/
5. Send us a message if you have trouble starting the server.

-----

## Problem statement:
- Our designer has designed the static html page for the messenger page. You
can access it by going to http://localhost:3000/static/messages-page.html
- You can see in the home page at http://localhost:3000/ that the data fetching
code is already implemented. The frontend app code exists in `app/` directory.
You can notice that as you change the file, the page gets updated automatically.
- Your problem statement is to implement the messaging app as per the
static page created by our designer at http://localhost:3000/static/messages-page.html

- A crude version of the conversations list is there in the `<ConversationsList />`
component. You can build on top of that to make the list look similar to
designer's static page.
- Build the rest of the page using react components. (You could create
a component for the right pane that contains the chat messages, and
chat entry box; and the chat list can use `ChatBubble` to show the messages,
and `ChatHead` component to show the profile pic.). It is up to you to decide
how well you organize your components.
- it should be possible to select a conversation and view all messages
in that conversation in the right side. (Read about 'state' in react to get
an idea about how to implement this).
- When new conversations come in, the conversation that the user selected
should remain selected until the user chooses to change it.
- Try to use the styles created by the designer inside your components,
so that if the component needs to be used in a different page in the site,
the styles will work out of the box, instead of having to add those styles
to a global stylesheet.
- Find out ways to use styles within the component, and not as part of global
`<styles>` tag.


## PS:[this is such an old task ignore the HOC , try to make the components as functional as possible]
- If you find the rate of incoming messages to be too noisy, you can change the
frequency of incoming messages by tweaking the value of `TIME_BETWEEN_NEW_MESSAGES`
to the number of seconds you want it to be.
- The `<AppContainer />` is a higher order component or HOC that does the data
fetching and passes data down to the child compoennt. Try to keep such data
related code in a higher order component, and don't use it to display any
actual markup other than just passing the data down to it's child. More info
about HOC [here](https://medium.com/@dan_abramov/mixins-are-dead-long-live-higher-order-components-94a0d2f9e750#.gj305z37u)
