const TIME_BETWEEN_NEW_MESSAGES = 2 * 1000 // Two seconds.

const people = [
  'Mark',
  'Isaac',
  'Surya',
  'Lindsey',
  'John',
  'Senthil',
  'Elon',
  'Larry',
  'Mary',
  'Vivek',
]

const sentences = [
  'Hey',
  'How is it going?',
  'Did you go for the movie yesterday?',
  'Free next weekend? Let\'s catch up!',
  'Coming for movie this weekend?',
  'Want to play a game of chess?',
  'Do you still have the Harry Potter movies collection?',
  'Don\'t forget to get the groceries',
  'Coming to watch Kabali this weekend? We got 2 tickets.',
  'Cycling to Mahabalipuram tomorrow at 5am. You in?',
  'New restaurant has opened up across the street. Let\'s check out tonight!',
  'Coming to Burger King for lunch?',
  'Did you get the gifts for tomorrow\'s event?',
  'Turn on the TV now :D',
]

let conversations = []

const getRandomMessage = () => {
  return sentences[ Math.floor( Math.random() * sentences.length ) ]
}

const getUniqueConversationID = () => {
  if (conversations.length === 0) {
    return 1000000
  }
  else {
    const max = conversations
      .reduce((acc, c) =>
        (c.ID > acc) ? c.ID : acc
      , 1000000)
    return max + 1
  }
}

const addRandomConversation = () => {
  const randomPerson = people[ Math.floor( Math.random() * 10 ) ]
  const x = conversations.filter(converstion => converstion.people.indexOf(randomPerson) !== -1)
  const message = getRandomMessage()

  if (x.length === 0) {
    conversations = [
      {
        ID: getUniqueConversationID(),
        people: [randomPerson],
        messages: [ message ],
      },
      ...conversations
    ]
  }
  else {
    const conversation = x[0]
    const conversationID = conversation.ID
    const index = conversations.map(c => c.ID).indexOf(conversationID)

    conversations = [
      {
        ...conversation,
        messages: [ ...conversation.messages, message ],
      },
      ...conversations.slice(0, index),
      ...conversations.slice(index+1),
    ]
  }
  console.log(randomPerson + ': ' + message)
}

const initRandomConversationAdder = () => {
  // Some time between 15-30 seconds
  const randomDuration = TIME_BETWEEN_NEW_MESSAGES + (Math.random() * TIME_BETWEEN_NEW_MESSAGES)
  setTimeout(() => {
    addRandomConversation()
    initRandomConversationAdder()
  }, randomDuration)
}

export default {
  addRandomConversation,
  getConversations: () => conversations,
  initRandomConversationAdder
}
