import React, { Component } from 'react'
import fetch from 'isomorphic-fetch'
import App from './App'
import ConversationsList from './ConversationsList'

export default class AppContainer extends Component {
  state = {
    conversations: []
  }
  componentWillMount() {
    this.fetchMessages()
    this.timer = setInterval(() => this.fetchMessages(), 1000)
  }
  compoentWillUnmount() {
    clearInterval(this.timer)
  }
  fetchMessages = () => {
    fetch('/api/conversations')
      .then(res => res.json())
      .then(json => this.setState({ conversations: json }))
  }
  render() {
    const { conversations } = this.state
    console.log(conversations)
    return <App conversations={ conversations } />
  }
}
