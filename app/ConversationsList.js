import React, { Component } from 'react'
import fetch from 'isomorphic-fetch'

export default class ConversationsList extends Component {
  render() {
    const { conversations } = this.props
    return (
      <ul>
        {conversations.map(conversation => (
          <li key={ conversation.ID }>
            {conversation.people[0]}
          </li>
        ))}
      </ul>
    )
  }
}
