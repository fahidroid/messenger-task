import React, { Component } from 'react'
import ConversationsList from './ConversationsList'

export default class App extends Component {
  render() {
    const { conversations } = this.props
    console.log(conversations)
    return (
      <div style={{ display: 'flex' }}>
        <div style={{ width: 170 }}>
        <ConversationsList conversations={ conversations } />
        </div>
        <pre style={{ flex: 1 }}>{JSON.stringify(conversations, null, ' ')}</pre>
      </div>
    )
  }
}
